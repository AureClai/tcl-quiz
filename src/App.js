import React, { useEffect, useState, useRef } from "react";
import MainAppBar from "./components/MainAppBar";
import { MapContainer, Marker, Popup, TileLayer, GeoJSON } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import "./App.css";
import metroGeoJSONbrut from "./data/sytral_tcl_sytral.tcllignemf_2_0_0.json";
// import busGeoJSON from "./data/sytral_tcl_sytral.tcllignebus_2_0_0.json";
import tramGeoJSONbrut from "./data/sytral_tcl_sytral.tcllignetram_2_0_0.json";
// import arretGeoJSON from "./data/sytral_tcl_sytral.tclarret.json";
import {
  Typography,
  FormControl,
  Select,
  InputLabel,
  MenuItem,
  Button,
  Stack,
  Modal,
  Box,
} from "@mui/material";


const toHexColor = (colorString) => {
  // Split the color string into individual components
  const rgb = colorString.split(" ");

  // Convert each component to a hexadecimal string
  const hex = rgb.map((component) => {
    // Parse the component as an integer and convert to hexadecimal
    const hexComponent = parseInt(component, 10).toString(16);

    // Pad with a zero if necessary (to ensure 2 digits)
    return hexComponent.length === 1 ? "0" + hexComponent : hexComponent;
  });

  // Join the components and prepend a '#'
  return "#" + hex.join("");
};

const metroGeoJSON = {
  ...metroGeoJSONbrut,
  features: metroGeoJSONbrut.features.filter((feat) => {
    return feat.properties.sens === "Aller";
  }),
};
const tramGeoJSON = {
  ...tramGeoJSONbrut,
  features: tramGeoJSONbrut.features.filter((feat) => {
    return feat.properties.sens === "Aller";
  }),
};

const choices = [
  ...metroGeoJSON.features.map((feat) => {
    return {
      id: feat.id,
      ligne: feat.properties.ligne,
      nom_trace: feat.properties.nom_trace,
    };
  }),
  ...tramGeoJSON.features.map((feat) => {
    return {
      id: feat.id,
      ligne: feat.properties.ligne,
      nom_trace: feat.properties.nom_trace,
    };
  }),
];

const maxVies = 3;

function App() {
  const [mapHeight, setMapHeight] = useState("300px"); // Fallback height
  const navBarRef = useRef(null);
  const [lineSelected, setLineSelected] = useState({});
  const [idGuessed, setIdGuessed] = useState("");
  const [vies, setVies] = useState(maxVies);
  const [score, setScore] = useState(0);
  const [GOModalOpen, setGOModalOpen] = useState(false);

  const updateMapHeight = () => {
    const navBarHeight = navBarRef.current.clientHeight;
    const viewportHeight = window.innerHeight;
    setMapHeight(`${viewportHeight - navBarHeight - 250}px`);
  };
  // console.log(arretGeoJSON);
  // console.log(metroGeoJSON);
  console.log(lineSelected);

  const changeSelected = (lastId) => {
    var newLine = { type: "", id: "" };
    var endProcess = false;
    while (!endProcess) {
      const randNum = Math.floor(
        Math.random() *
          (metroGeoJSON.features.length + tramGeoJSON.features.length)
      );
      if (randNum < metroGeoJSON.features.length) {
        newLine = {
          type: "metro",
          id: metroGeoJSON.features[randNum].id,
        };
      } else {
        newLine = {
          type: "tram",
          id: tramGeoJSON.features[randNum - metroGeoJSON.features.length].id,
        };
      }

      if (newLine.id !== lastId) {
        endProcess = true;
      }
    }
    setLineSelected(newLine);
  };

  const onGuessChange = (e) => {
    setIdGuessed(e.target.value);
  };

  useEffect(() => {
    changeSelected();
    updateMapHeight();
    window.addEventListener("resize", updateMapHeight);

    return () => {
      window.removeEventListener("resize", updateMapHeight);
    };
  }, []);

  const responseVerification = (e) => {
    if (idGuessed === lineSelected.id) {
      setScore(score + 1);
      changeSelected(lineSelected.id);
    } else {
      setVies(vies - 1);
      if (vies === 1) {
        setGOModalOpen(true);
      } else {
        changeSelected(lineSelected.id);
      }
    }
  };

  const handleGOModalClose = (e) => {
    setScore(0);
    setVies(maxVies);
    changeSelected("");
    setGOModalOpen(false);
  };

  return (
    <div className="App">
      <div ref={navBarRef}>
        <MainAppBar />
      </div>
      <div style={{ height: mapHeight }}>
        <MapContainer
          key={`${mapHeight}-${lineSelected.id}`}
          center={[45.75, 4.85]}
          zoom={12}
          scrollWheelZoom={true}
          style={{ height: mapHeight }} // Directly use mapHeight for the MapContainer height
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {/* <GeoJSON
            data={busGeoJSON}
            onEachFeature={(feature, layer) => {
              const defaultStyle = {
                color: toHexColor("200 200 200"),
                weight: 2,
              };
              layer.setStyle(defaultStyle);
              const popupOptions = {
                minWidth: 100,
                maxWidth: 250,
                className: "popup-ligne",
              };

              layer.bindPopup(() => {
                return `<b>${feature.properties.ligne} : ${feature.properties.nom_trace}</b>`;
              }, popupOptions);

              layer.on("mouseover", (e) => {
                layer.setStyle({
                  color: toHexColor("100 100 100"),
                  weight: 6,
                });
              });

              layer.on("mouseout", (e) => {
                layer.setStyle(defaultStyle);
              });
            }}
          /> */}
          {lineSelected.type === "metro" ? (
            <GeoJSON
              data={metroGeoJSON}
              onEachFeature={(feature, layer) => {
                if (feature.id === lineSelected.id) {
                  layer.setStyle({
                    color: toHexColor(feature.properties.couleur),
                    weight: 4,
                  });
                } else {
                  layer.setStyle({
                    opacity: 0,
                  });
                }

                // const popupOptions = {
                //   minWidth: 100,
                //   maxWidth: 250,
                //   className: "popup-ligne",
                // };

                // layer.bindPopup(() => {
                //   return `<b>${feature.properties.ligne} : ${feature.properties.nom_trace}</b>`;
                // }, popupOptions);

                // layer.on("mouseover", (e) => {
                //   layer.setStyle({ weight: 6 });
                // });

                // layer.on("mouseout", (e) => {
                //   layer.setStyle({ weight: 4 });
                // });
              }}
            />
          ) : (
            ""
          )}

          {lineSelected.type === "tram" ? (
            <GeoJSON
              data={tramGeoJSON}
              onEachFeature={(feature, layer) => {
                if (feature.id === lineSelected.id) {
                  layer.setStyle({
                    color: toHexColor(feature.properties.couleur),
                    weight: 4,
                  });
                } else {
                  layer.setStyle({
                    opacity: 0,
                  });
                }

                // const popupOptions = {
                //   minWidth: 100,
                //   maxWidth: 250,
                //   className: "popup-ligne",
                // };

                // layer.bindPopup(() => {
                //   return `<b>${feature.properties.ligne} : ${feature.properties.nom_trace}</b>`;
                // }, popupOptions);

                // layer.on("mouseover", (e) => {
                //   layer.setStyle({ weight: 6 });
                // });

                // layer.on("mouseout", (e) => {
                //   layer.setStyle({ weight: 4 });
                // });
              }}
            />
          ) : (
            ""
          )}
          <Marker position={[51.505, -0.09]}>
            <Popup>
              A pretty CSS3 popup. <br /> Easily customizable.
            </Popup>
          </Marker>
        </MapContainer>
      </div>
      <div style={{ height: "200px" }}>
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          spacing={2}
          margin={2}
        >
          <Typography variant="h4">
            Vies : {vies}/{maxVies}
          </Typography>
          <Typography variant="h4">Score : {score}</Typography>
        </Stack>

        <Typography variant="h6" margin={1}>
          Quelle est cette ligne ?
        </Typography>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Ligne</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={idGuessed}
            label="Age"
            onChange={onGuessChange}
          >
            {choices.map((elem) => {
              return (
                <MenuItem key={elem.id} value={elem.id}>
                  {elem.ligne}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <Button
          onClick={responseVerification}
          style={{ marginTop: "20px" }}
          variant="contained"
        >
          Valider
        </Button>
      </div>
      <Modal
        open={GOModalOpen}
        onClose={handleGOModalClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            width: 400,
            bgcolor: "background.paper",
            border: "2px solid #000",
            boxShadow: 24,
            p: 4,
          }}
        >
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Game Over !
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Score : {score}
          </Typography>
          <Button onClick={handleGOModalClose} variant="contained" style={{margin: "10px"}}>
            Rejouer
          </Button>
        </Box>
      </Modal>
    </div>
  );
}

export default App;
