import { ThemeOptions } from '@mui/material/styles';

export const themeOptions: ThemeOptions = {
  palette: {
    mode: 'light',
    primary: {
      main: '#e2001a',
    },
    secondary: {
      main: '#f50057',
    },
    background: {
      default: '#f1f5f5',
    },
  },
};