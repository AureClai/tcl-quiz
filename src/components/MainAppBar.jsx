import {
  Box,
  AppBar,
  Toolbar,
  Typography,
} from "@mui/material";
// import MenuIcon from "@mui/icons-material/Menu";
import React from "react";

export default function MainAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            TCL Quiz v0.0.1alpha
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
